package cn.com.sandi.wechatcontrol.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName wcmini_info
 */
@TableName(value ="wcmini_info")
@Data
public class WcminiInfo implements Serializable {
    /**
     * 
     */
    @TableId
    private Long autoid;

    /**
     * 
     */
    private String appid;

    /**
     * 
     */
    private String appsecret;

    /**
     * 
     */
    private String appname;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}