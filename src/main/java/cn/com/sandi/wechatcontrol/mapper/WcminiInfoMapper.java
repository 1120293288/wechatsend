package cn.com.sandi.wechatcontrol.mapper;

import cn.com.sandi.wechatcontrol.model.WcminiInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
* @author Administrator
* @description 针对表【wcmini_info】的数据库操作Mapper
* @createDate 2023-04-10 02:21:35
* @Entity cn.com.sandi.wechatcontrol.model.WcminiInfo
*/
@Repository
public interface WcminiInfoMapper extends BaseMapper<WcminiInfo> {

}




