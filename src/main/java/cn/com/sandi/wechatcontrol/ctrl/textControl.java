package cn.com.sandi.wechatcontrol.ctrl;

import cn.com.sandi.wechatcontrol.utils.CommonUtils;
import cn.com.sandi.wechatcontrol.utils.QuestionNaireQrCodeUtils;
import cn.com.sandi.wechatcontrol.utils.RedisUtils;
import cn.com.sandi.wechatcontrol.utils.RequestUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * @Description //TODO
 * @Date 2023/4/10 9:41
 * @Author hjs
 **/
@RestController
@Component
@Slf4j
public class textControl {

    @Resource
    private RedisUtils redisUtils;

    @RequestMapping(value = "/textRedis")
    public String text(){

        Object wx03f4cd32b530dae2 = redisUtils.get("wx03f4cd32b530dae2");
//        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
//        RedisTemplate RedisTemplateConfig = (RedisTemplate)context.getBean("RedisTemplate");
//        String o = RedisTemplateConfig.opsForValue().get("wx03f4cd32b530dae2").toString();
        JSONObject jsonObject = JSONObject.parseObject(JSON.toJSONString(wx03f4cd32b530dae2));
        String token = jsonObject.getString("token");
        byte[] qrcode = QuestionNaireQrCodeUtils.getQuestionNaireQrcode(47L, token,"pages/questionConclusion/questionConclusion", "trial");
        String filePath = CommonUtils.getPropertiesValue("application", "oweReportPath");
        //生成图片
        String imgUrl = CommonUtils.makeImg(filePath, qrcode);
        //String sendUrl = "http://..."+imgUrl;
        //发送img
        if (imgUrl != null){
//            RequestUtils.sendPicMsg("wxid_ye13igmg42z622", imgUrl, "30001");
        }
        log.info(token);
        return token;
    }
}
