package cn.com.sandi.wechatcontrol.config;

import cn.com.sandi.wechatcontrol.utils.CommonUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class CrosConfig implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        String oweReportPathMap = CommonUtils.getPropertiesValue("application", "oweReportPathMap");
        String oweReportPath = CommonUtils.getPropertiesValue("application", "oweReportPath");
        registry.addResourceHandler(oweReportPathMap+"/**").addResourceLocations("file:"+oweReportPath);
    }
}
