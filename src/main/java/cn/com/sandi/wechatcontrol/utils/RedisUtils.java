package cn.com.sandi.wechatcontrol.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
@Component
public class RedisUtils {
    @Autowired
    private RedisTemplate redisTemplate;
    /**
     * ���w?�s����??
     * @param key  ?
     * @param time ??(��)
     */
    public boolean expire(String key, long time) {
        try {
            if (time > 0) {
                redisTemplate.expire(key, time, TimeUnit.SECONDS);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    /**
     * ���ukey ?��?��??
     * @param key ? ����?null
     * @return ??(��) ��^0�N��?�ä[����
     */
    public long getExpire(String key) {
        return redisTemplate.getExpire(key, TimeUnit.SECONDS);
    }
    /**
     * �P?key�O�_�s�b
     * @param key ?
     * @return true �s�b false���s�b
     */
    public boolean hasKey(String key) {
        try {
            return redisTemplate.hasKey(key);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    /**
     * ?��?�s
     * @param key �i�H?�@?�� �Φh?
     */
    @SuppressWarnings("unchecked")
    public void del(String... key) {
        if (key != null && key.length > 0) {
            if (key.length == 1) {
                redisTemplate.delete(key[0]);
            } else {
                redisTemplate.delete(String.valueOf(CollectionUtils.arrayToList(key)));
            }
        }
    }
    // ============================String=============================
    /**
     * ���q?�s?��
     * @param key ?
     * @return ��
     */
    public Object get(String key) {
        return key == null ? null : redisTemplate.opsForValue().get(key);
    }
    /**
     * ���q?�s��J
     * @param key   ?
     * @param value ��
     * @return true���\ false��?
     */
    public boolean set(String key, Object value) {
        try {
            redisTemplate.opsForValue().set(key, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    /**
     * ���q?�s��J�}?�m??
     * @param key   ?
     * @param value ��
     * @param time  ??(��) time�n�j�_0 �p�Gtime�p�_���_0 ??�m?����
     * @return true���\ false ��?
     */
    public boolean set(String key, Object value, long time) {
        try {
            if (time > 0) {
                redisTemplate.opsForValue().set(key, value, time, TimeUnit.SECONDS);
            } else {
                set(key, value);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    /**
     * ?�W
     * @param key   ?
     * @param delta �n�W�[�L(�j�_0)
     */
    public long incr(String key, long delta) {
        if (delta < 0) {
            throw new RuntimeException("?�W�]�l��?�j�_0");
        }
        return redisTemplate.opsForValue().increment(key, delta);
    }
    /**
     * ??
     * @param key   ?
     * @param delta �n?�֤L(�p�_0)
     */
    public long decr(String key, long delta) {
        if (delta < 0) {
            throw new RuntimeException("??�]�l��?�j�_0");
        }
        return redisTemplate.opsForValue().increment(key, -delta);
    }
    // ================================Map=================================
    /**
     * HashGet
     * @param key  ? ����?null
     * @param item ? ����?null
     */
    public Object hget(String key, String item) {
        return redisTemplate.opsForHash().get(key, item);
    }
    /**
     * ?��hashKey??���Ҧ�?��
     * @param key ?
     * @return ??���h??��
     */
    public Map<Object, Object> hmget(String key) {
        return redisTemplate.opsForHash().entries(key);
    }
    /**
     * HashSet
     * @param key ?
     * @param map ??�h??��
     */
    public boolean hmset(String key, Map<String, Object> map) {
        try {
            redisTemplate.opsForHash().putAll(key, map);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    /**
     * HashSet �}?�m??
     * @param key  ?
     * @param map  ??�h??��
     * @param time ??(��)
     * @return true���\ false��?
     */
    public boolean hmset(String key, Map<String, Object> map, long time) {
        try {
            redisTemplate.opsForHash().putAll(key, map);
            if (time > 0) {
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    /**
     * �V�@?hash����J?�u,�p�G���s�b??��
     *
     * @param key   ?
     * @param item  ?
     * @param value ��
     * @return true ���\ false��?
     */
    public boolean hset(String key, String item, Object value) {
        try {
            redisTemplate.opsForHash().put(key, item, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    /**
     * �V�@?hash����J?�u,�p�G���s�b??��
     *
     * @param key   ?
     * @param item  ?
     * @param value ��
     * @param time  ??(��) �`�N:�p�G�w�s�b��hash��??,?��??��?�즳��??
     * @return true ���\ false��?
     */
    public boolean hset(String key, String item, Object value, long time) {
        try {
            redisTemplate.opsForHash().put(key, item, value);
            if (time > 0) {
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    /**
     * ?��hash������
     *
     * @param key  ? ����?null
     * @param item ? �i�H�Ϧh? ����?null
     */
    public void hdel(String key, Object... item) {
        redisTemplate.opsForHash().delete(key, item);
    }
    /**
     * �P?hash���O�_��??����
     *
     * @param key  ? ����?null
     * @param item ? ����?null
     * @return true �s�b false���s�b
     */
    public boolean hHasKey(String key, String item) {
        return redisTemplate.opsForHash().hasKey(key, item);
    }
    /**
     * hash?�W �p�G���s�b,�N??�ؤ@? �}��s�W�Z���Ȫ�^
     *
     * @param key  ?
     * @param item ?
     * @param by   �n�W�[�L(�j�_0)
     */
    public double hincr(String key, String item, double by) {
        return redisTemplate.opsForHash().increment(key, item, by);
    }
    /**
     * hash??
     *
     * @param key  ?
     * @param item ?
     * @param by   �n?��?(�p�_0)
     */
    public double hdecr(String key, String item, double by) {
        return redisTemplate.opsForHash().increment(key, item, -by);
    }
    // ============================set=============================
    /**
     * ���ukey?��Set�����Ҧ���
     * @param key ?
     */
    public Set<Object> sGet(String key) {
        try {
            return redisTemplate.opsForSet().members(key);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    /**
     * ���uvalue?�@?set���d?,�O�_�s�b
     *
     * @param key   ?
     * @param value ��
     * @return true �s�b false���s�b
     */
    public boolean sHasKey(String key, Object value) {
        try {
            return redisTemplate.opsForSet().isMember(key, value);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    /**
     * ??�u��Jset?�s
     *
     * @param key    ?
     * @param values �� �i�H�O�h?
     * @return ���\??
     */
    public long sSet(String key, Object... values) {
        try {
            return redisTemplate.opsForSet().add(key, values);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }


    /**
     * ?set?�u��J?�s
     *
     * @param key    ?
     * @param time   ??(��)
     * @param values �� �i�H�O�h?
     * @return ���\??
     */
    public long sSetAndTime(String key, long time, Object... values) {
        try {
            Long count = redisTemplate.opsForSet().add(key, values);
            if (time > 0)
                expire(key, time);
            return count;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }


    /**
     * ?��set?�s��?��
     *
     * @param key ?
     */
    public long sGetSetSize(String key) {
        try {
            return redisTemplate.opsForSet().size(key);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }


    /**
     * ������?value��
     *
     * @param key    ?
     * @param values �� �i�H�O�h?
     * @return ������??
     */

    public long setRemove(String key, Object... values) {
        try {
            Long count = redisTemplate.opsForSet().remove(key, values);
            return count;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
    // ===============================list=================================
    /**
     * ?��list?�s��?�e
     *
     * @param key   ?
     * @param start ?�l
     * @param end   ?�� 0 �� -1�N��Ҧ���
     */
    public List<Object> lGet(String key, long start, long end) {
        try {
            return redisTemplate.opsForList().range(key, start, end);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    /**
     * ?��list?�s��?��
     *
     * @param key ?
     */
    public long lGetListSize(String key) {
        try {
            return redisTemplate.opsForList().size(key);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
    /**
     * �q?���� ?��list������
     *
     * @param key   ?
     * @param index ���� index>=0?�A 0 ��?�A1 �ĤG?�����A�̦�?���Findex<0?�A-1�A����A-2��?�ĤG?�����A�̦�?��
     */
    public Object lGetIndex(String key, long index) {
        try {
            return redisTemplate.opsForList().index(key, index);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    /**
     * ?list��J?�s
     *
     * @param key   ?
     * @param value ��
     */
    public boolean lSet(String key, Object value) {
        try {
            redisTemplate.opsForList().rightPush(key, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    /**
     * ?list��J?�s
     * @param key   ?
     * @param value ��
     * @param time  ??(��)
     */
    public boolean lSet(String key, Object value, long time) {
        try {
            redisTemplate.opsForList().rightPush(key, value);
            if (time > 0)
                expire(key, time);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }
    /**
     * ?list��J?�s
     *
     * @param key   ?
     * @param value ��
     * @return
     */
    public boolean lSet(String key, List<Object> value) {
        try {
            redisTemplate.opsForList().rightPushAll(key, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }
    /**
     * ?list��J?�s
     *
     * @param key   ?
     * @param value ��
     * @param time  ??(��)
     * @return
     */
    public boolean lSet(String key, List<Object> value, long time) {
        try {
            redisTemplate.opsForList().rightPushAll(key, value);
            if (time > 0)
                expire(key, time);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    /**
     * ���u���ޭק�list�����Y??�u
     *
     * @param key   ?
     * @param index ����
     * @param value ��
     * @return
     */
    public boolean lUpdateIndex(String key, long index, Object value) {
        try {
            redisTemplate.opsForList().set(key, index, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    /**
     * ����N?��?value
     *
     * @param key   ?
     * @param count �����h��?
     * @param value ��
     * @return ������??
     */
    public long lRemove(String key, long count, Object value) {
        try {
            Long remove = redisTemplate.opsForList().remove(key, count, value);
            return remove;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
}
