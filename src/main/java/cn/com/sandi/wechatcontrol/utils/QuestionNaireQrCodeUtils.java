package cn.com.sandi.wechatcontrol.utils;


import cn.com.sandi.wechatcontrol.mapper.WcminiInfoMapper;
import com.alibaba.fastjson.JSONObject;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.nio.charset.StandardCharsets;
@Component
public class QuestionNaireQrCodeUtils {
    protected static Logger logger = Logger.getLogger(QuestionNaireQrCodeUtils.class);

    private static String wechatQrcodeUrl = "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=";
//    private static final String appid = "wxce53ee95c1a11048";
//    private static final String secret = "c17e9db806e016c7988e75850ab51f18";
    private static String wechatAccessTokenUrl = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s";




    private static WcminiInfoMapper wcminiInfoMapper;

    @Autowired
    private WcminiInfoMapper wcminiInfoMapperAuto;

    @PostConstruct
    private void init() {
        wcminiInfoMapper = this.wcminiInfoMapperAuto;
    }

    /**
     * 获取请求二维码的token
     * @return token
     */
//    public static String getAccessToken() {
//        String url = String.format(wechatAccessTokenUrl, appid, secret);
//        HttpURLConnection httpURLConnection = null;
//        try {
//            httpURLConnection = (HttpURLConnection) new URL(url).openConnection();
//            httpURLConnection.setRequestMethod("POST");
//            // 发送POST请求必须设置如下两行
//            httpURLConnection.setDoOutput(true);
//            httpURLConnection.setDoInput(true);
//            // 获取URLConnection对象对应的输出流
//            PrintWriter printWriter = new PrintWriter(httpURLConnection.getOutputStream());
//            // 发送请求参数
////            printWriter.write(JSONObject.toJSONString(body));
//            // flush输出流的缓冲
//            printWriter.flush();
//            //开始获取数据
//            try (InputStream inputStream = httpURLConnection.getInputStream();
//                 ByteArrayOutputStream out = new ByteArrayOutputStream()) {
//                byte[] buffer = new byte[1024];
//                int len = -1;
//                while ((len = inputStream.read(buffer)) != -1) {
//                    out.write(buffer, 0, len);
//                }
//                JSONObject jsonObject = new JSONObject();
//                jsonObject.put("data",out.toString());
//                return jsonObject.getJSONObject("data").getString("access_token");
//            }
//        } catch (Exception e) {
//            logger.error("",e);
//            throw new RuntimeException(e);
//        }finally {
//            if (httpURLConnection != null) {
//                httpURLConnection.disconnect();
//            }
//        }
//    }

    /**
     * 获取问卷的二维码
     * @param quid
     * @return
     */
    public static byte[] getQuestionNaireQrcode(Long quid,String token,String page,String level){

        RestTemplate restTemplate = new RestTemplate();

        restTemplate.getMessageConverters().set(1, new StringHttpMessageConverter(StandardCharsets.UTF_8));
        JSONObject obj = new JSONObject();
        //传参数
        obj.put("scene", quid);
        //默认是主页，页面 page，例如 pages/index/index，根路径前不要填加 /，不能携带参数（参数请放在 scene 字段里），如果不填写这个字段，scene获取不到，默认跳主页面。
        obj.put("page", page);
        //自动配置线条颜色，如果颜色依然是黑色，则说明不建议配置主色调，默认 false
        obj.put("auto_color", true);
        // 正式版为 "release"，体验版为 "trial"，开发版为 "develop"。默认是正式版。
        obj.put("env_version", level);
        //为 true 时 page 必须是已经发布的小程序存在的页面（否则报错）
        obj.put("check_path", false);
        // 二维码底色透明
        obj.put("is_hyaline", true);
        String str = obj.toString();

        //发送请求
        ResponseEntity<byte[]> responseEntity = restTemplate.postForEntity("https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token="+token , str, byte[].class);

        String type = responseEntity.getHeaders().getContentType().getType();

        if ("image".equals(type)) {
            //返回地是图片
            byte[] fileData = responseEntity.getBody();
            return fileData;
        } else {//返回的是errCode的json串
            return new byte[0];
        }
    }
}
