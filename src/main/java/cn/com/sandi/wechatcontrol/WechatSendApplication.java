package cn.com.sandi.wechatcontrol;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import javax.annotation.PostConstruct;

@Slf4j
@SpringBootApplication
@MapperScan("cn.com.sandi.wechatcontrol.mapper")
public class WechatSendApplication {
    @Autowired
    private ApplicationContext appContextaa;

    private static ApplicationContext applicationContext;

    @PostConstruct
    public void init(){
        applicationContext = appContextaa;
    }
    public static void main(String[] args) {

        SpringApplication.run(WechatSendApplication.class, args);
//        String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
//        for (String beanDefinitionName : beanDefinitionNames) {
//            log.info(beanDefinitionName);
//        }
    }

}
