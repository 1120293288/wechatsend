package cn.com.sandi.wechatcontrol.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.com.sandi.wechatcontrol.model.WcminiInfo;
import cn.com.sandi.wechatcontrol.service.WcminiInfoService;
import cn.com.sandi.wechatcontrol.mapper.WcminiInfoMapper;
import org.springframework.stereotype.Service;

/**
* @author Administrator
* @description 针对表【wcmini_info】的数据库操作Service实现
* @createDate 2023-04-10 02:21:35
*/
@Service
public class WcminiInfoServiceImpl extends ServiceImpl<WcminiInfoMapper, WcminiInfo>
    implements WcminiInfoService{

}




