package cn.com.sandi.wechatcontrol.service;

import cn.com.sandi.wechatcontrol.model.WcminiInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Administrator
* @description 针对表【wcmini_info】的数据库操作Service
* @createDate 2023-04-10 02:21:35
*/
public interface WcminiInfoService extends IService<WcminiInfo> {

}
