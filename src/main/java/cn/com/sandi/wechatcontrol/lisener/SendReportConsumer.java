package cn.com.sandi.wechatcontrol.lisener;


import cn.com.sandi.wechatcontrol.mapper.CubeAnswersheetMapper;
import cn.com.sandi.wechatcontrol.mapper.WcminiInfoMapper;
import cn.com.sandi.wechatcontrol.model.MessageModel;
import cn.com.sandi.wechatcontrol.model.WcminiInfo;
import cn.com.sandi.wechatcontrol.utils.CommonUtils;
import cn.com.sandi.wechatcontrol.utils.QuestionNaireQrCodeUtils;
import cn.com.sandi.wechatcontrol.utils.RedisUtils;
import cn.com.sandi.wechatcontrol.utils.RequestUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Slf4j
@Component
public class SendReportConsumer {

    @Resource
    private RedisUtils redisUtils;

    @Autowired
    private CubeAnswersheetMapper cubeAnswersheetMapper;

    @Autowired
    private WcminiInfoMapper wcminiInfoMapper;

    @JmsListener(destination = "${sendReportQueue}-${sendReportQueueWxidOne}")
    public void receSendReportQueueMsgOne(MessageModel messageModel){
        log.info("处理参数:"+messageModel);
        switch (messageModel.getTitle()){
            case "firstIntoGroup" :
                handleReportQueueMsg(messageModel);
                break;
            default: break;
        }
    }
    @JmsListener(destination = "${sendReportQueue}-${sendReportQueueWxidTwo}")
    public void receSendReportQueueMsgTwo(MessageModel messageModel){
        log.info("处理参数:"+messageModel);
        switch (messageModel.getTitle()){
            case "firstIntoGroup" :
                handleReportQueueMsg(messageModel);
                break;
            default: break;
        }
    }
    public void handleReportQueueMsg(MessageModel messageModel){

        WcminiInfo wcminiInfo = wcminiInfoMapper.selectById(1);
        if (wcminiInfo == null) {
            log.error("获取小程序appid失败！");
            return;
        }
//      WcMiniTimerMoniterService类的openTask_initAppInfoToRedis()方法 每两小时刷新一次access_token 存在redis
        String wcmininfoJsonStr = JSON.toJSONString(redisUtils.get(wcminiInfo.getAppid()));
        if (StringUtils.isNotBlank(wcmininfoJsonStr)){
            JSONObject wcMiniInfoJSON = JSONObject.parseObject(wcmininfoJsonStr);
            String token = wcMiniInfoJSON.getString("token");
            log.info("小程序token: "+token);
            byte[] qrcode = QuestionNaireQrCodeUtils.getQuestionNaireQrcode(Long.valueOf(messageModel.getAnswersheetId()), token,"pages/questionConclusion/questionConclusion", "trial");
            if (qrcode.length > 0) {
                //获取图片存储目录
                String filePath = CommonUtils.getPropertiesValue("application", "oweReportPath");
                //获取图片映射路径
                String oweReportPathMap = CommonUtils.getPropertiesValue("application", "oweReportPathMap");
                String localUrl = CommonUtils.getPropertiesValue("application", "localUrl");
                //生成图片
                String imgName = CommonUtils.makeImg(filePath, qrcode);
                //String sendUrl = "http://..."+linux映射路径+imgUrl;
                //发送img
                if (imgName != null){
                    String sendUrl = localUrl + oweReportPathMap+ "/"+imgName;
                    boolean res = RequestUtils.sendPicMsg(messageModel.getWxid(), sendUrl,imgName, messageModel.getServerPort());
                    if (!res){
                        log.error("发送二维码失败,msg:"+messageModel);
                    }
                }else {
                    log.error("生成二维码失败，msg:"+messageModel);
                }
            }else {
                log.error("调用微信二维码接口失败,msg:"+messageModel);
            }
        }else {
            log.error("获取该appid的teken为空,appid:"+wcminiInfo.getAppid());
        }
    }

}
